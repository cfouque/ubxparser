/*!
 * \file UbxDefinitions.hpp
 * \author Cl??ment Fouque
 * \version 1.0
 * \date 26/05/2011
 */

#ifndef __UBLOX_UBX_DEFINITION__
#define __UBLOX_UBX_DEFINITION__

#include <vector>
#include <map>
#include <bitset>

// #include <QtEndian>
// #include <QVariant>

#include "ubx_datatype.hpp"

namespace ublox
{

//! Sets the verbosoity of the parser
const bool VERBOSE_PARSER = false ;

//! Size of UBX header message
const uint16_t HEADER_SIZE = 6 ;

//! Size of CRC Check
const uint16_t CRC_SIZE = 2 ;

//! Number of Receiver channel
const uint16_t NB_CHANNEL = 50 ;

//! First synchronisation char for UBX Frame
const U1 SYNC1 = 0xB5 ;

//! Second synchronisation char for UBX Frame
const U1 SYNC2 = 0x62 ;


/*!
 * \brief compute message id
 * Compute a uniique msg id from msg_cl and msg_id
 */
constexpr MSG_ID getMsgUniqueId( const U1 & msg_cl , const U1 & msg_id )
{
    return msg_cl * 256 + msg_id ;
}

MSG_ID getMsgUniqueId( const buffer_t & buffer );

/*!
 * \brief get the payload size
 * This function extracts the payload size from the buffer.
 */
U2 payloadSize( const buffer_t * pbuffer );
U2 payloadSize( std::shared_ptr<buffer_t> pbuffer );

/*!
 * \brief computes the message size from buffer
 * Payload size + header + CRC
 * */
U2 messageSize( const buffer_t * pbuffer ) ;
U2 messageSize( std::shared_ptr<buffer_t> pbuffer ) ;

/*!
 * \brief Check if the message is CRC valid.
 */
bool checkCRC( const buffer_t * pbuffer ) ;
bool checkCRC( std::shared_ptr<buffer_t> pbuffer ) ;

/*!
 * \brief Compute the values for CRC.
 */
void computeCRC( buffer_v & crc1 , buffer_v & crc2 , const buffer_t * pbuffer ) ;
void computeCRC( buffer_v & crc1 , buffer_v & crc2 , std::shared_ptr<buffer_t> pbuffer ) ;

/*!
 * \brief Corrects the subframe.s
 */
I4 subframeCorrection( I4 subframe ) ;

/*!
 * \brief get the Message name
 */
std::string msgIdAsString( const U1 & msg_cl , const U1 & msg_id ) ;

/*!
 * \brief generates the poll request message
 * This function generates the poll request message with an empty payload
 * if the message is pollable.
 */
buffer_t generatePollRequest( const U1 & m_cl = 0 , const U1 & m_id = 0 );

} // End of namespace ublox

// Ubx inclusion:
#include "buffer_conversion.hpp"
#include "ubx_ack_definitions.hpp"
#include "ubx_aid_definitions.hpp"
#include "ubx_cfg_definitions.hpp"
#include "ubx_esf_definitions.hpp"
#include "ubx_inf_definitions.hpp"
#include "ubx_mon_definitions.hpp"
#include "ubx_nav_definitions.hpp"
#include "ubx_rxm_definitions.hpp"
#include "ubx_tim_definitions.hpp"

#endif
// kate: indent-mode cstyle; indent-width 4; replace-tabs on;
