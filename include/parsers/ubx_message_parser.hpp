/*!
 * \author cfouque
 * \date 2018/04/28
 */

#ifndef __UBLOX_MESSAGE_PARSER_HPP__
#define __UBLOX_MESSAGE_PARSER_HPP__

#include "ubx_datatype.hpp"
#include "ubx_definitions.hpp"

namespace ublox
{

template <typename data_t>
class MessageParser
{
    const U1 m_msgCl;
    const U1 m_msgId;

public :
    explicit MessageParser( const U1 & cl, const U1 & id ):
        m_msgCl(cl),
        m_msgId(id)
        {}

    U1 msgClass() const { return m_msgCl; }
    U1 msgId() const { return m_msgId; }
    MSG_ID id() const { return getMsgUniqueId(msgClass(),msgId()); }

    virtual bool readBuffer( const buffer_t & buffer, data_t * data = nullptr ) const = 0;

protected:

};



} // end namespace ublox

#endif

