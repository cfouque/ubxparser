#ifndef __UBLOX_UBX_ACK_PARSER__
#define __UBLOX_UBX_ACK_PARSER__

#include "ubx_message_parser.hpp"

namespace ublox
{
namespace ack
{

class Parser final : public ublox::MessageParser<AckData>
{
public :
    explicit Parser() : MessageParser(ublox::AID,ublox::ACK) {}
    bool readBuffer( const buffer_t & buffer , AckData * data = nullptr ) const override;
};

} // end namespace ack
} // end namespace ublox

#endif
