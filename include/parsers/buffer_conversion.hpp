#ifndef __BUFFER_CONVERSION__
#define __BUFFER_CONVERSION__

#include <sstream>
#include <vector>
#include <list>
#include <bitset>
#include <iostream>

#include "ubx_datatype.hpp"

/*!
 * \brief Converts to ublox::U1
 * \var startByte
 * \var pbuffer
 *
 * Converts the buffer to ublox::U1 starting from startByte.
 */
bool extractBit( char byte, int pos ) ;
ublox::U1 bufferToUChar( int startByte , const ublox::buffer_t * pbuffer ) ;
ublox::U2 bufferToUShort( int startByte , const ublox::buffer_t * pbuffer ) ;
ublox::U4 bufferToULong( int startByte , const ublox::buffer_t * pbuffer ) ;
ublox::I1 bufferToChar( int startByte , const ublox::buffer_t * pbuffer );
ublox::I2 bufferToShort( int startByte , const ublox::buffer_t * pbuffer );
ublox::I4 bufferToLong( int startByte , const ublox::buffer_t * pbuffer );
ublox::R4 bufferToFloat( int startByte , const ublox::buffer_t * pbuffer ) ;
ublox::R8 bufferToDouble( int startByte , const ublox::buffer_t * pbuffer ) ;
ublox::X1 bufferToX1( int startByte , const ublox::buffer_t * pbuffer );
ublox::X2 bufferToX2( int startByte , const ublox::buffer_t * pbuffer );
ublox::X4 bufferToX4( int startByte , const ublox::buffer_t * pbuffer );

bool extractBit( char byte, int pos ) ;
ublox::U1 bufferToUChar( int startByte , std::shared_ptr<ublox::buffer_t> pbuffer ) ;
ublox::U2 bufferToUShort( int startByte , std::shared_ptr<ublox::buffer_t>pbuffer ) ;
ublox::U4 bufferToULong( int startByte , std::shared_ptr<ublox::buffer_t>pbuffer ) ;
ublox::I1 bufferToChar( int startByte , std::shared_ptr<ublox::buffer_t> pbuffer );
ublox::I2 bufferToShort( int startByte , std::shared_ptr<ublox::buffer_t> pbuffer );
ublox::I4 bufferToLong( int startByte , std::shared_ptr<ublox::buffer_t> pbuffer );
ublox::R4 bufferToFloat( int startByte , std::shared_ptr<ublox::buffer_t> pbuffer ) ;
ublox::R8 bufferToDouble( int startByte , std::shared_ptr<ublox::buffer_t> pbuffer ) ;
ublox::X1 bufferToX1( int startByte , std::shared_ptr<ublox::buffer_t> pbuffer );
ublox::X2 bufferToX2( int startByte , std::shared_ptr<ublox::buffer_t> pbuffer );
ublox::X4 bufferToX4( int startByte , std::shared_ptr<ublox::buffer_t> pbuffer );

ublox::U1  extractSFID( const long int * DWRD ) ;
ublox::U2 extractTOW( const long int * DWRD ) ;
ublox::U2 extractSVID( const long int * DWRD ) ;

void UCharToBuffer( ublox::U1 val , unsigned int startByte , ublox::buffer_t * p_buffer ) ;
void UShortToBuffer( ublox::U2 val , unsigned int startByte , ublox::buffer_t * pbuffer ) ;
void ShortToBuffer( ublox::I2 val , uint startByte , ublox::buffer_t * pbuffer ) ;
void ULongToBuffer( ublox::U4 val , uint startByte , ublox::buffer_t * pbuffer ) ;
void LongToBuffer( ublox::I4 val , uint startByte , ublox::buffer_t * pbuffer ) ;
void FloatToBuffer( ublox::R4 val , uint startByte , ublox::buffer_t * pbuffer ) ;
void DoubleToBuffer( ublox::R8 val , uint startByte , ublox::buffer_t * pbuffer ) ;
void X1ToBuffer( ublox::X1 val , uint startByte , ublox::buffer_t * pbuffer ) ;
void X2ToBuffer( ublox::X2 val , uint startByte , ublox::buffer_t * pbuffer ) ;
void X4ToBuffer( ublox::X4 val , uint startByte , ublox::buffer_t * pbuffer ) ;


#endif
