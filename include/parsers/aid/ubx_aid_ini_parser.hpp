#ifndef __UBLOX_AID_INI_PARSER__
#define __UBLOX_AID_INI_PARSER__

#include "ubx_message_parser.hpp"

namespace ublox
{
namespace aid
{

class IniParser final : public MessageParser<IniData>
{

public :
    explicit IniParser() : MessageParser<IniData>( AID,INI ) {} ;
    bool readBuffer( const buffer_t & buffer, IniData * data = nullptr ) const override;
} ;

} // end namespace aid
} // end namespace ublox

#endif

