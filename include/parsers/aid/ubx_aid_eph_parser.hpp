/*!
 * \file UbxAidEphParser.hpp
 * \author Cl??ment Fouque
 * \date 27-07-2011
 * \version 2
 *
 * This file contains the declaration for ublox::aid::HuiParser
 *
 */

#ifndef __UBLOX_AID_EPH_PARSER__
#define __UBLOX_AID_EPH_PARSER__

#include "ubx_message_parser.hpp"

namespace ublox
{
namespace aid
{

class EphParser final : public ublox::MessageParser<EphData>
{
public :
    explicit EphParser() : MessageParser(AID,EPH) {}

    bool readBuffer( const buffer_t & buffer, EphData * data = nullptr ) const override;
    buffer_t generatePollRequest( U1 svid );
} ;

} // end namespace aid
} // end namespace ublox

#endif
