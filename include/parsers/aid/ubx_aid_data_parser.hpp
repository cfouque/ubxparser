/*!
 * \file UbxAidDataParser.hpp
 * \author Cl??ment Fouque
 * \date 27-07-2011
 * \version 2
 *
 * This file defines a aid::ReqParser
 *
 */

#ifndef __UBLOX_AID_DATA_PARSER__
#define __UBLOX_AID_DATA_PARSER__

#include "UbxDefinitions.hpp"
#include "base/ubx_message_parser.hpp"

namespace ublox
{
namespace aid
{

/*!
 * \brief Poll GPS initial aiding data
 *
 * If this poll is received, the messages AID-INI, AID-HUI, AID-EPH and AID-ALM are sent.
 */

class DataParser final : public MessageParser<NullData>
{
public :
    explicit DataParser() : MessageParser<NullData>( AID , DATA , true ) { };
    void processFrame( std::shared_ptr<buffer_t> pbuff )
    {
        doneProcessing() ;
    } ;

} ;

} // end namespace aid
} // end namespace ublox

#endif
