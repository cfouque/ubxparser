#ifndef __UBLOX_AID_ALM_PARSER__
#define __UBLOX_AID_ALM_PARSER__

#include "ubx_message_parser.hpp"

namespace ublox
{
namespace aid
{

class AlmParser final : public ublox::MessageParser<AlmData>
{

public :
    explicit AlmParser() : MessageParser( ublox::AID, ublox::aid::ALM ) {}

    bool readBuffer( const buffer_t & buffer, AlmData * data = nullptr ) const override;
    buffer_t generatePollRequest( U1 svid ) ;
} ;

} // end namespace aid
} // end namespace ublox

#endif

