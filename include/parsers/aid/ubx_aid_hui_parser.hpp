/*!
 * \file UbxAidHuiParser.hpp
 * \author Cl??ment Fouque
 * \date 27-07-2011
 * \version 2
 *
 * This file defines the parser for AID-HUI data
 *
 */

#ifndef __UBLOX_AID_HUI_PARSER__
#define __UBLOX_AID_HUI_PARSER__

#include "ubx_message_parser.hpp"

namespace ublox
{
namespace aid
{

class HuiParser final : public ublox::MessageParser<HuiData>
{
public :
    explicit HuiParser() : MessageParser( AID,HUI) { }
    bool readBuffer( const buffer_t & buffer, HuiData * data = nullptr ) const override;
};

} // end namespace aid
} // end namespace ublox

#endif
