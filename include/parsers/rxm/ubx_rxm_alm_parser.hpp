#ifndef __UBLOX_RXM_ALM_PARSER__
#define __UBLOX_RXM_ALM_PARSER__


#include "ubx_message_parser.hpp"

namespace ublox
{
namespace rxm
{

class AlmParser final : public MessageParser<AlmData>
{

public :
    explicit AlmParser() : MessageParser<AlmData>( RXM,ALM ) {} ;
    bool readBuffer( const buffer_t & buffer, AlmData * data = nullptr ) const override;
    buffer_t generatePollRequest( U1 svid ) ;
} ;

} // end namespace rxm
} // end namespace ublox

#endif
