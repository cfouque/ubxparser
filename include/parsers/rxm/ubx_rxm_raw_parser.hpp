#ifndef __UBLOX_UBX_RXM_RAW_PARSER__
#define __UBLOX_UBX_RXM_RAW_PARSER__


#include "ubx_message_parser.hpp"

namespace ublox
{
namespace rxm
{

class RawParser final : public MessageParser<RawData>
{

public :
    explicit RawParser() : MessageParser<RawData>( RXM,RAW ) {}
    bool readBuffer( const buffer_t & buffer, RawData * data = nullptr ) const override;
} ;

} // end namespace rxm
} // end namespace ublox

#endif
