#ifndef __UBLOX_UBX_RXM_SVSI_PARSER__
#define __UBLOX_UBX_RXM_SVSI_PARSER__

#include "ubx_message_parser.hpp"

namespace ublox
{
namespace rxm
{

class SvsiParser final : public MessageParser<SvsiData>
{
public :
    explicit SvsiParser() : MessageParser<SvsiData>( RXM,SVSI ) {}
    bool readBuffer( const buffer_t & buffer, SvsiData * data = nullptr ) const override;

} ;

} // end namespace rxm
} // end namespace ublox

#endif
