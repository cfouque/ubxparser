/*!
 * \file UbxRxmEphParser.hpp
 * \author Cl??ment Fouque
 * \date 27-07-2011
 * \version 2
 *
 * This file contains the declaration for ublox::rxm::EphParser
 *
 */

#ifndef __UBLOX_RXM_EPH_PARSER__
#define __UBLOX_RXM_EPH_PARSER__


#include "ubx_message_parser.hpp"

namespace ublox
{
namespace rxm
{

class EphParser final : public MessageParser<EphData>
{
public :
    explicit EphParser() : MessageParser<EphData>( RXM,EPH ) {}
    bool readBuffer( const buffer_t & buffer, EphData * data = nullptr ) const override;
    buffer_t generatePollRequest( U1 svid );
};

} // end namespace rxm
} // end namespace ublox

#endif
