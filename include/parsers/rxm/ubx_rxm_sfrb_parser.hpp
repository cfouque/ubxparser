#ifndef __UBLOX_UBX_RXM_SFRB_PARSER__
#define __UBLOX_UBX_RXM_SFRB_PARSER__

#include "ubx_message_parser.hpp"

namespace ublox
{
namespace rxm
{

class SfrbParser final : public MessageParser<SfrbData>
{

public :
    explicit SfrbParser( ) : MessageParser<SfrbData>( RXM,SFRB ) { }

    bool readBuffer( const buffer_t & buffer, SfrbData * data = nullptr ) const override ;

private :
    void assignSbasBuffer( SfrbData::Block * block ) const;
} ;

} // end namespace rxm
} // end namespace ublox

#endif
