/*!
 * \file UbxRxmReqParser.hpp
 * \author Cl??ment Fouque
 * \date 27-07-2011
 * \version 2
 *
 * This file defines a aid::ReqParser
 *
 */

#ifndef __UBLOX_RXM_PMREQ_PARSER__
#define __UBLOX_RXM_PMREQ_PARSER__

#include "ubx_message_parser.hpp"

namespace ublox
{
namespace rxm
{

class PmreqParser final : public MessageParser<PmreqData>
{
public :
    explicit PmreqParser() : MessageParser<PmreqData>( RXM , PMREQ ) {}

    bool readBuffer( const buffer_t & buffer, PmreqData * data = nullptr ) const override { return false; }

    buffer_t generatePmRequest( U4 duration , bool backup ) ;
} ;

} // end namespace rxm
} // end namespace ublox

#endif
