#ifndef __UBLOX_UBX_NAV_SOL_PARSER__
#define __UBLOX_UBX_NAV_SOL_PARSER__

#include "ubx_message_parser.hpp"

namespace ublox
{
namespace nav
{

class SolParser final : public MessageParser<SolData>
{
public :
    explicit SolParser() : MessageParser<SolData>( NAV,SOL ) {}
    bool readBuffer( const buffer_t & buffer, SolData * data = nullptr ) const override;
} ;

} // end namespace nav
} // end namespace ublox

#endif
