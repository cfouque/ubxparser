#ifndef __UBLOX_UBX_NAV_SBAS_PARSER__
#define __UBLOX_UBX_NAV_SBAS_PARSER__

#include "ubx_message_parser.hpp"

namespace ublox
{
namespace nav
{

class SbasParser final : public MessageParser<SbasData>
{
public :
    explicit SbasParser() : MessageParser<SbasData>( NAV,SBAS ) {} ;
    bool readBuffer( const buffer_t & buffer, SbasData * data = nullptr ) const override;
} ;

} // end namespace nav
} // end namespace ublox

#endif
