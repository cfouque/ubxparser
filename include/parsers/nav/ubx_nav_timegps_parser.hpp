#ifndef __UBLOX_UBX_NAV_TIMEGPS_PARSER__
#define __UBLOX_UBX_NAV_TIMEGPS_PARSER__

#include "ubx_message_parser.hpp"

namespace ublox
{
namespace nav
{

class TimegpsParser final : public MessageParser<TimegpsData>
{
public :
    explicit TimegpsParser( ) : MessageParser<TimegpsData>( NAV,TIMEGPS ) {} ;
    bool readBuffer( const buffer_t & buffer, TimegpsData * data = nullptr ) const override;

} ;

} // end namespace nav
} // end namespace ublox

#endif
