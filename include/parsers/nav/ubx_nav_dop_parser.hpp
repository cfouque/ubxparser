#ifndef __UBLOX_UBX_NAV_DOP_PARSER__
#define __UBLOX_UBX_NAV_DOP_PARSER__

 
#include "ubx_message_parser.hpp"

namespace ublox
{
namespace nav
{

class DopParser final : public MessageParser<DopData>
{
public :
    explicit DopParser() : MessageParser<DopData>( NAV , DOP ) {} ;
    bool readBuffer( const buffer_t & buffer, DopData * data = nullptr ) const override;
} ;

} // end namespace nav
} // end namespace ublox

#endif
