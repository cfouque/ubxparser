#ifndef __UBLOX_UBX_NAV_POSECEF_PARSER__
#define __UBLOX_UBX_NAV_POSECEF_PARSER__

 
#include "ubx_message_parser.hpp"

namespace ublox
{
namespace nav
{

class PosecefParser final : public MessageParser<PosecefData>
{
public :
    explicit PosecefParser() : MessageParser<PosecefData>( NAV,POSECEF ) {} ;
    bool readBuffer( const buffer_t & buffer, PosecefData * data = nullptr ) const override;

} ;

} // end namespace nav
} // end namespace ublox

#endif
