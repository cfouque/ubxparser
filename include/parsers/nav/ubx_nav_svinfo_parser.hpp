#ifndef __UBLOX_UBX_NAV_SVINFO_PARSER__
#define __UBLOX_UBX_NAV_SVINFO_PARSER__

#include "ubx_message_parser.hpp"

namespace ublox
{
namespace nav
{

class SvinfoParser final : public MessageParser<SvinfoData>
{
public :
    explicit SvinfoParser() : MessageParser<SvinfoData>( NAV,SVINFO ) {}
    bool readBuffer( const buffer_t & buffer, SvinfoData * data = nullptr ) const override;
} ;

} // end namespace nav
} // end namespace ublox

#endif
