#ifndef __UBLOX_UBX_NAV_CLOCK_PARSER__
#define __UBLOX_UBX_NAV_CLOCK_PARSER__

 
#include "ubx_message_parser.hpp"

namespace ublox
{
namespace nav
{

class ClockParser final : public MessageParser<ClockData>
{
public :
    explicit ClockParser() : MessageParser<ClockData>( NAV , CLOCK ) {} ;
    bool readBuffer( const buffer_t & buffer, ClockData * data = nullptr ) const override;

} ;

} // end namespace nav
} // end namespace ublox

#endif
