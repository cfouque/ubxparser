#ifndef __UBLOX_UBX_NAV_AOPSTATUS_PARSER__
#define __UBLOX_UBX_NAV_AOPSTATUS_PARSER__

 
#include "ubx_message_parser.hpp"

namespace ublox
{
namespace nav
{

class AopstatusParser final : public MessageParser<AopstatusData>
{
public :
    explicit AopstatusParser() : MessageParser<AopstatusData>( NAV, AOPSTATUS ) {}

    bool readBuffer( const buffer_t & buffer, AopstatusData * data = nullptr ) const override;

} ;

} // end namespace nav
} // end namespace ublox

#endif
