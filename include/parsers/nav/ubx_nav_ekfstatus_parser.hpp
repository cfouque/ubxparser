#ifndef __UBLOX_UBX_NAV_EKFSTATUS_PARSER__
#define __UBLOX_UBX_NAV_EKFSTATUS_PARSER__

 
#include "ubx_message_parser.hpp"

namespace ublox
{
namespace nav
{

class EkfstatusParser final : public MessageParser<EkfstatusData>
{
public :
    explicit EkfstatusParser() : MessageParser<EkfstatusData>( NAV , EKFSTATUS ) {} ;
    bool readBuffer( const buffer_t & buffer, EkfstatusData * data = nullptr ) const override;

} ;

} // end namespace nav
} // end namespace ublox

#endif
