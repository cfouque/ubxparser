#ifndef __UBLOX_UBX_NAV_VELNED_PARSER__
#define __UBLOX_UBX_NAV_VELNED_PARSER__

#include "ubx_message_parser.hpp"

namespace ublox
{
namespace nav
{

class VelnedParser final : public MessageParser<VelnedData>
{
public :
    explicit VelnedParser() : MessageParser<VelnedData>( NAV,VELNED ) {} ;
    bool readBuffer( const buffer_t & buffer, VelnedData * data = nullptr ) const override;
} ;

} // end namespace nav
} // edn namespace ublox

#endif
