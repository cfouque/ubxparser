#ifndef __UBLOX_UBX_NAV_VELECEF_PARSER__
#define __UBLOX_UBX_NAV_VELECEF_PARSER__

#include "ubx_message_parser.hpp"

namespace ublox
{
namespace nav
{

class VelecefParser final : public MessageParser<VelecefData>
{
public :
    explicit VelecefParser( ) : MessageParser<VelecefData>( NAV,VELECEF ) {} ;
    bool readBuffer( const buffer_t & buffer, VelecefData * data = nullptr ) const override;
} ;

} // end namespace nav
} // end namespace ublox

#endif
