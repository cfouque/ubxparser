#ifndef __UBLOX_UBX_NAV_DGPS_PARSER__
#define __UBLOX_UBX_NAV_DGPS_PARSER__

 
#include "ubx_message_parser.hpp"

namespace ublox
{
namespace nav
{

class DgpsParser final : public MessageParser<DgpsData>
{
public :
    explicit DgpsParser() : MessageParser<DgpsData>( NAV , DGPS ) {} ;
    bool readBuffer( const buffer_t & buffer, DgpsData * data = nullptr ) const override;

} ;

} // end namespace nav
} // end namespace ublox

#endif
