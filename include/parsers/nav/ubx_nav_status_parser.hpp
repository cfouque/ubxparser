#ifndef __UBLOX_UBX_NAV_STATUS_PARSER__
#define __UBLOX_UBX_NAV_STATUS_PARSER__

#include "ubx_message_parser.hpp"

namespace ublox
{
namespace nav
{

class StatusParser final : public MessageParser<StatusData>
{
public :
    explicit StatusParser() : MessageParser<StatusData>( NAV , STATUS ) {}
    bool readBuffer( const buffer_t & buffer, StatusData * data = nullptr ) const override;
} ;

} // end namespace nav
} // end namespace ublox

#endif
