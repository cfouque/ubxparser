#ifndef __UBLOX_UBX_NAV_POSLLH_PARSER__
#define __UBLOX_UBX_NAV_POSLLH_PARSER__

#include "ubx_message_parser.hpp"

namespace ublox
{
namespace nav
{

class PosllhParser final : public MessageParser<PosllhData>
{
public :
    explicit PosllhParser() : MessageParser<PosllhData>( NAV,POSLLH ) {} ;
    bool readBuffer( const buffer_t & buffer, PosllhData * data = nullptr ) const override;

} ;

} // end namespace nav
} // end namespace ublox

#endif
