#ifndef __UBLOX_UBX_NAV_TIMEUTC_PARSER__
#define __UBLOX_UBX_NAV_TIMEUTC_PARSER__

#include "ubx_message_parser.hpp"

namespace ublox
{
namespace nav
{

class TimeutcParser final : public MessageParser<TimeutcData>
{
public :
    explicit TimeutcParser() : MessageParser<TimeutcData>( NAV , TIMEUTC ) {}
    bool readBuffer( const buffer_t & buffer, TimeutcData * data = nullptr ) const override;
} ;

} // end namespace nav
} // end namespace ublox

#endif
