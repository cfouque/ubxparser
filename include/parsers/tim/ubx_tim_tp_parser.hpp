#ifndef __UBLOX_UBX_TIM_TP_PARSER__
#define __UBLOX_UBX_TIM_TP_PARSER__

#include "ubx_message_parser.hpp"

namespace ublox
{
namespace tim
{

class TpParser final : public MessageParser<TpData>
{
public :
    explicit TpParser() : MessageParser<TpData>( TIM,TP ) {}
    bool readBuffer( const buffer_t & buffer, TpData * data = nullptr ) const override;

} ;

} // end namespace tim
} // end namespace ublox

#endif
