#ifndef __UBLOX_UBX_TIM_SVIN_PARSER__
#define __UBLOX_UBX_TIM_SVIN_PARSER__

#include "ubx_message_parser.hpp"

namespace ublox
{
namespace tim
{

class SvinParser final : public MessageParser<SvinData>
{
public :
    explicit SvinParser() : MessageParser<SvinData>( TIM,SVIN ) {}
    bool readBuffer( const buffer_t & buffer, SvinData * data = nullptr ) const override;

} ;

} // end namespace tim
} // end namespace ublox

#endif
