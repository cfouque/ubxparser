#ifndef __UBLOX_UBX_TIM_TM2_PARSER__
#define __UBLOX_UBX_TIM_TM2_PARSER__

#include "ubx_message_parser.hpp"

namespace ublox
{
namespace tim
{

class Tm2Parser final : public MessageParser<Tm2Data>
{
public :
    explicit Tm2Parser() : MessageParser<Tm2Data>( TIM,TM2 ) {}
    bool readBuffer( const buffer_t & buffer, Tm2Data * data = nullptr ) const override;

} ;

} // end namespace tim
} // end namespace ublox

#endif
