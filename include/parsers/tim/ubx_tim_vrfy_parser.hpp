#ifndef __UBLOX_UBX_TIM_VRFY_PARSER__
#define __UBLOX_UBX_TIM_VRFY_PARSER__

#include "ubx_message_parser.hpp"

namespace ublox
{
namespace tim
{

class VrfyParser final : public MessageParser<VrfyData>
{
public :
    explicit VrfyParser() : MessageParser<VrfyData>( TIM,VRFY ) {}
    bool readBuffer( const buffer_t & buffer, VrfyData * data = nullptr ) const override;

} ;

} // end namespace tim
} // end namespace ublox

#endif
