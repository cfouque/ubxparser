#ifndef __UBLOX_CFG_SBAS_PARSER__
#define __UBLOX_CFG_SBAS_PARSER__

#include "ubx_message_parser.hpp"

namespace ublox
{
namespace cfg
{

class SbasParser final : public MessageParser<SbasData>
{

public :

    explicit SbasParser() : MessageParser<SbasData>( CFG,SBAS ) {}

    bool readBuffer( const buffer_t & buffer, SbasData * data = nullptr ) const override ;

    buffer_t getConfigurationMsg( const SbasData * config ) ;

} ;

} // end namespace cfg
} // end namespace ublox

#endif
