#ifndef __UBLOX_CFG_MSG_PARSER__
#define __UBLOX_CFG_MSG_PARSER__

#include "ubx_message_parser.hpp"

namespace ublox
{
namespace cfg
{

class MsgParser final : public MessageParser<MsgData>
{

public :

    MsgParser() : MessageParser<MsgData>(CFG,MSG) {}

    bool readBuffer( const buffer_t & buffer, MsgData * data = nullptr ) const override;

    buffer_t getConfigurationMsg( const MsgData & config ) ;
    buffer_t getConfigurationMsg( U1 msgClass , U1 msgId , U1 rate , cfg::RX_PORT port );

} ;

} // end namespace config
} // end namespace ublox

#endif
