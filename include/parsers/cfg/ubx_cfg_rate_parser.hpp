#ifndef __UBLOX_CFG_RATE_PARSER__
#define __UBLOX_CFG_RATE_PARSER__

#include "ubx_message_parser.hpp"

namespace ublox
{
namespace cfg
{

class RateParser final : public MessageParser<RateData>
{

public :

    RateParser() : MessageParser<RateData>( CFG,RATE ) {}

    bool readBuffer( const buffer_t & buffer, RateData * data = nullptr ) const override ;

    buffer_t getConfigurationMsg( const RateData * config ) ;
    buffer_t getConfigurationMsg( U2 measRate , U2 navRate , TIME_REF timeRef ) ;

} ;

}
}

#endif
