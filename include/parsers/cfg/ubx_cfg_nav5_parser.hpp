#ifndef __UBLOX_CFG_NAV5_PARSER__
#define __UBLOX_CFG_NAV5_PARSER__

#include "ubx_message_parser.hpp"

namespace ublox
{
namespace cfg
{

class Nav5Parser final : public MessageParser<Nav5Data>
{

public :

    explicit Nav5Parser() : MessageParser<Nav5Data>( CFG,NAV5 ) {}

    bool readBuffer( const buffer_t & buffer, Nav5Data * data = nullptr ) const override;

    buffer_t getConfigurationMsg( const Nav5Data * config ) ;

} ;

} // end namespace cfg
} // end namespace ublox

#endif
