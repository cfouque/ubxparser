/*!
 * \file UbxBaseData.hpp
 * \author Cl??ment Fouque
 * \date 27-07-2011
 * \version 2
 *
 * This file defines the base class for all data structure
 *
 */

#ifndef __UBLOX_BASE_DATA__
#define __UBLOX_BASE_DATA__

/**
 * \file UbxBaseData.hpp
 * \author Clement fouque
 * \date 25/05/2011
 * \version 1.0
 *
 * \brief Abstract class to handle UBX data structure
 *
 * This files contains an abstract class definition to be used as a basis
 * for all data structure coming from an UBX message.
 *
 * */

#include <mutex>
#include "ubx_datatype.hpp"

namespace ublox
{
//! \class BaseData
//! \brief Virtual class for data class
//! This abstract class should be inherited by class that describes
//! ublox messages. \n

class BaseData
{
public :
    //! Constructor
    //! Takes MSG_CLASS and MSG_ID as argument
    explicit BaseData( const char & cl , const char & id ) :
        iTow( 0 ),
        week( 0 ),
        m_mutex()
    {
        msg_clss = cl ;
        msg_id = id ;
        m_mutex.unlock();
    } ;

    //! Overloaded copy constructor
    BaseData( const BaseData & base ) :
        m_mutex()
    {
        msg_clss = base.msg_clss ;
        msg_id = base.msg_id ;
        iTow = base.iTow ;
        week = base.week ;
    } ;

    //! Default Destructor
    virtual ~BaseData() {} ;

    //! Class of the message
    char msg_clss ;

    //! ID of the message
    char msg_id ;

    //! GPS Timestamp of the MSG (ms)
    U4 iTow ;

    //! GPS Week of the MSG
    U2 week ;

    //! returns the timestamp of the msg
    inline double timestamp()
    {
        return 1e-3 * static_cast<double>( this->iTow ) ;
    } ;

    //! Display data as a human-readable format (for debugging purpose)
    virtual void display() {} ;

    //! \brief Abstract member function to reset class members
    //! This abstract member function must be reimplemented by classes
    //! that inherits from ublox::BaseData.
    virtual void clear() = 0 ;

    //! lock the mutex
    void lock()
    {
        m_mutex.lock();
    }

    //! Try to acquire the lock without blocking.
    bool try_lock()
    {
        return m_mutex.try_lock();
    }

    //! Release the lock.
    void unlock()
    {
        m_mutex.unlock();
    }

private :
    //! a mutex to allow concurential access to data.
    std::mutex m_mutex ;

} ;

/*!
 * \brief Empty data structure used for convenience
 */
class NullData final : public BaseData
{
public :
    NullData() : BaseData( 0,0 ) {};
    void clear() {};
} ;

} // end namespace ublox

#endif
