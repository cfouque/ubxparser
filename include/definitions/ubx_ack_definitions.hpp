#ifndef __UBLOX_UBX_ACK_DEFINITIONS__
#define __UBLOX_UBX_ACK_DEFINITIONS__

#include <iostream>
#include <vector>
#include <bitset>

#include "ubx_base_data.hpp"
#include "ubx_datatype.hpp"

namespace ublox
{

const U1 ACK = 0x05 ;

namespace ack
{

const U1 ACK = 0x01 ; //! Message acknoledged
const U1 NAK = 0x00 ; //! Message not acknoledged

class AckData final : public ublox::BaseData
{

public :
    explicit AckData() : ublox::BaseData( ACK, ack::ACK )
    {
        this->clear();
    }

    bool acknowledged ;
    I1 clsID ;
    I1 msgID ;

    void clear() override
    {
        acknowledged = false ;
        clsID = 0 ;
        msgID = 0 ;
    };
};

} // end namespace ack
} // end namespace ublox

#endif
