#ifndef __UBLOX_UBX_ESF_DEFINITIONS__
#define __UBLOX_UBX_ESF_DEFINITIONS__

#include "ubx_base_data.hpp"
#include "ubx_datatype.hpp"

namespace ublox
{

const I1 ESF = 0x10 ;

namespace esf
{
const I1 MEAS = 0x02 ;
const I1 STATUS = 0x10 ;

class MeasData ;
class StatusData ;

} // end namespace esf
} // end namespace ublox

#endif
