#ifndef __UBLOX_UBX_RXM_DEFINITIONS__
#define __UBLOX_UBX_RXM_DEFINITIONS__

#include "ubx_base_data.hpp"
#include "ubx_datatype.hpp"

namespace ublox
{

const I1 RXM = 0x02 ;

namespace rxm
{

const unsigned int MAX_NUMBER_SFRB_BLOCKS = 500 ;

const I1 ALM = 0x30 ;
const I1 EPH = 0x31 ;
const I1 PMREQ = 0x41 ;
const I1 RAW = 0x10 ;
const I1 SFRB = 0x11 ;
const I1 SVSI = 0x20 ;

class AlmData final : public BaseData
{
public :
    explicit AlmData() :
        ublox::BaseData( RXM,ALM )
    {
        clear() ;
    };
    void clear() override ;

    U4 svid;
    U4 almWeek;
    std::array<U4,8> dwrd ;
} ;

class EphData final : public BaseData
{
public :
    explicit EphData() :
        ublox::BaseData( RXM,EPH )
    {
        clear();
    }
    void clear() override;

    U4 svid;
    U4 how;
    std::array<U4,8> sf1d;
    std::array<U4,8> sf2d;
    std::array<U4,8> sf3d;
} ;

class PmreqData final : public BaseData
{
public :
    PmreqData() : ublox::BaseData( RXM,PMREQ )
    {
        clear();
    };
    void clear() override;

    U4 duration ;
    X4 flags ;
} ;

//! \struct RawData
//! Handles the measurements provided by the RXM-RAW message
class RawData final : public BaseData
{
public :
    explicit RawData() :
        ublox::BaseData( RXM,RAW )
    {
        clear();
    };

    void clear() override;
    void display() override;

    struct MeasBlock
    {
        U1 svId ;     //! PRN of the sv
        R8 L1 ;           //! Carrier-phase (L1 Cycles)
        R8 C1 ;           //! Pseudorange (m)
        R4 D1 ;           //! Doppler (Hz)
        I1 QualIndic ;        //! Measurements quality indicator
        I1 snr ;          //! Signal Strength
        I1 LLI ;          //! Loss-of-Lock Indicator
    };

    U1 numSV ;        //! Number of available measurements
    std::vector<MeasBlock> meas ;      //! Vector of measurements blocks
} ;


//! \struct SFRBData
class SfrbData final : public BaseData
{
public :
    explicit SfrbData() :
        ublox::BaseData( RXM,SFRB )
    {
        clear();
    }

    void clear() override;

    struct Block
    {
        U1 chn ;
        U1 svId ;
        std::array<I4,10> dwrd;
        std::vector<I1> sbas_buffer ;
    };

    std::vector<Block> dwrds ;
};

//! Container for UBX-SVSI messages
class SvsiData final : public BaseData
{
public :
    explicit SvsiData() :
        ublox::BaseData( RXM,SVSI )
    {
        clear();
    }

    void clear() override;

    struct Block
    {
        U1 svid ;
        U1 svFlag ;
        I2 azim ;
        U1 elev ;
        U1 age ;
    };

    U1 numVis ;
    U1 numSV ;
    std::vector<Block> svInfos ;
};

} // end ublox::nav
} // end ublox

#endif
