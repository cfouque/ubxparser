#ifndef __UBLOX_DATATYPE__
#define __UBLOX_DATATYPE__

#include <bitset>
#include <memory>
#include <vector>
#include <array>
#include <functional>
#include <cstdint>

#include <boost/signals2/signal.hpp>

namespace ublox
{
// Type of message fields
typedef uint8_t U1 ;
typedef uint16_t U2 ;
typedef uint32_t U4 ;
typedef char I1 ;
typedef int16_t I2 ;
typedef int32_t I4 ;
typedef float R4 ;
typedef double R8 ;
typedef std::bitset<8> X1 ;
typedef std::bitset<16> X2 ;
typedef std::bitset<32> X4 ;

typedef char buffer_v ;
typedef std::vector<buffer_v> buffer_t ;
typedef boost::signals2::signal<void ()> signal_t ;
typedef signal_t::slot_type slot_t ;
typedef uint16_t MSG_ID ;

class BaseData ;
typedef std::shared_ptr<BaseData> data_p ;

class AbstractParser ;
typedef std::shared_ptr<AbstractParser> parser_p ;

}

#endif

