#include "tim/ubx_tim_tp_parser.hpp"

namespace ublox
{
namespace tim
{

bool TpParser::readBuffer( const buffer_t & buffer, TpData * data ) const
{
    data->towMS = bufferToULong( HEADER_SIZE,&buffer ) ;
    data->towSubMS = bufferToULong( HEADER_SIZE+4,&buffer );
    data->qErr = bufferToLong( HEADER_SIZE+8,&buffer );
    data->week = bufferToUShort( HEADER_SIZE+12,&buffer );
    data->flags = bufferToX1( HEADER_SIZE+14,&buffer );

    return true;
}

}
}
