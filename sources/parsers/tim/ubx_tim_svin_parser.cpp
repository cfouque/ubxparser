#include "tim/ubx_tim_svin_parser.hpp"

namespace ublox
{
namespace tim
{

bool SvinParser::readBuffer( const buffer_t & buffer, SvinData * data ) const
{
    data->dur = bufferToULong( HEADER_SIZE,&buffer );
    data->meanX = bufferToLong( HEADER_SIZE+4,&buffer );
    data->meanY = bufferToLong( HEADER_SIZE+8,&buffer );
    data->meanZ = bufferToLong( HEADER_SIZE+12,&buffer );
    data->meanV = bufferToULong( HEADER_SIZE+16,&buffer );
    data->obs = bufferToULong( HEADER_SIZE+20,&buffer );
    data->valid = bufferToUChar( HEADER_SIZE+24,&buffer );
    data->active = bufferToUChar( HEADER_SIZE+25,&buffer ) ;

    return true;
}

}
}
