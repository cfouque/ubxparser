#include "tim/ubx_tim_vrfy_parser.hpp"

namespace ublox
{
namespace tim
{

bool VrfyParser::readBuffer( const buffer_t & buffer, VrfyData * data ) const
{
    data->itow = bufferToLong( HEADER_SIZE,&buffer ) ;
    data->frac = bufferToLong( HEADER_SIZE+4,&buffer );
    data->deltaMs = bufferToLong( HEADER_SIZE+8,&buffer ) ;
    data->deltaNs = bufferToLong( HEADER_SIZE+12,&buffer );
    data->wno = bufferToUShort( HEADER_SIZE+16,&buffer ) ;
    data->flags = bufferToX1( HEADER_SIZE+18,&buffer );

    return true;
}

}
}
