#include "tim/ubx_tim_tm2_parser.hpp"

namespace ublox
{
namespace tim
{

bool Tm2Parser::readBuffer( const buffer_t & buffer, Tm2Data * data ) const
{
    data->ch = bufferToUChar( HEADER_SIZE,&buffer ) ;
    data->flags = bufferToX1( HEADER_SIZE+1,&buffer );
    data->count = bufferToUShort( HEADER_SIZE+2,&buffer );
    data->wnR = bufferToUShort( HEADER_SIZE+4,&buffer );
    data->wnF = bufferToUShort( HEADER_SIZE+6,&buffer );
    data->towMsR = bufferToULong( HEADER_SIZE+8,&buffer ) ;
    data->towSubMsR = bufferToULong( HEADER_SIZE+12,&buffer );
    data->towMsF = bufferToULong( HEADER_SIZE+16,&buffer );
    data->towSubMsF = bufferToULong( HEADER_SIZE+20,&buffer ) ;
    data->accEst = bufferToULong( HEADER_SIZE+24,&buffer ) ;

    return true;
}

}
}
