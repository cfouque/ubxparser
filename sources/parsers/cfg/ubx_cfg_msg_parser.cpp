#include "cfg/ubx_cfg_msg_parser.hpp"

namespace ublox
{
namespace cfg
{

bool MsgParser::readBuffer ( const buffer_t & buffer, MsgData * data ) const
{
    data->msgClass = bufferToUChar ( HEADER_SIZE , &buffer );
    data->msgID = bufferToUChar ( HEADER_SIZE+1 , &buffer );

    if ( payloadSize ( &buffer ) == 3 )
    {
        data->rate = bufferToUChar ( HEADER_SIZE+2 , &buffer );
    }
    else if ( payloadSize ( &buffer ) == 8 )
    {
        for ( unsigned int k = 0 ; k < 6 ; ++k )
        {
            data->rates[k] = bufferToUChar ( HEADER_SIZE+2+k , &buffer );
        }
    }

    return true;
}

buffer_t MsgParser::getConfigurationMsg ( const MsgData & config )
{
    buffer_t buf ( 16,char ( 0 ) ) ;
    buf[0] = SYNC1 ;
    buf[1] = SYNC2 ;
    buf[2] = msgClass();
    buf[3] = msgId();
    UShortToBuffer ( 8,4,&buf );
    UCharToBuffer ( config.msgClass , 6 , &buf ) ;
    UCharToBuffer ( config.msgID , 7 , &buf ) ;

    for ( unsigned int k = 0 ; k < 6 ; ++k )
        UCharToBuffer ( config.rates[k] , k+8 , &buf );

    computeCRC ( buf[14] , buf[15] , &buf );
    return buf ;
}

buffer_t MsgParser::getConfigurationMsg ( U1 msgClass , U1 msgId , U1 rate , RX_PORT port )
{
    buffer_t buf ( 16,char ( 0 ) ) ;
    buf[0] = SYNC1 ;
    buf[1] = SYNC2 ;
    buf[2] = this->msgClass() ;
    buf[3] = this->msgId() ;
    UShortToBuffer ( 8,4,&buf );
    UCharToBuffer ( msgClass , 6 , &buf ) ;
    UCharToBuffer ( msgId , 7 , &buf ) ;

    if ( port == ALL )
    {
        for ( unsigned int k = 8 ; k < 13 ; ++k )
        {
            UCharToBuffer ( rate , k , &buf );
        }
    }
    else
    {
        UCharToBuffer ( rate , port+8 , &buf );
    }

    computeCRC ( buf[14] , buf[15] , &buf );
    return buf ;
}

}
}
