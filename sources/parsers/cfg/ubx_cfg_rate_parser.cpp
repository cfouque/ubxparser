#include "cfg/ubx_cfg_rate_parser.hpp"

namespace ublox
{
namespace cfg
{


bool RateParser::readBuffer( const buffer_t & buffer, RateData * data ) const
{
    data->measRate = bufferToUShort( HEADER_SIZE , &buffer );
    data->navRate = bufferToUShort( HEADER_SIZE+2 , &buffer );
    data->timeRef = bufferToUShort( HEADER_SIZE+4 , &buffer ) ;
    return true;
}

buffer_t RateParser::getConfigurationMsg( U2 measRate , U2 navRate , TIME_REF timeRef )
{
    RateData config;
    config.measRate = measRate ;
    config.navRate = navRate ;
    config.timeRef = timeRef ;
    return RateParser::getConfigurationMsg( &config ) ;
}

buffer_t RateParser::getConfigurationMsg( const RateData * config )
{
    buffer_t buf( HEADER_SIZE+6+CRC_SIZE,char( 0 ) ) ;
    buf[0] = SYNC1 ;
    buf[1] = SYNC2 ;
    buf[2] = CFG ;
    buf[3] = RATE ;
    UShortToBuffer( 6 , 4 , &buf );
    UShortToBuffer( config->measRate , 6 , &buf );
    UShortToBuffer( config->navRate , 8 , &buf );
    UShortToBuffer( config->timeRef , 10 , &buf );
    computeCRC( buf[12] , buf[13] , &buf );
    return buf ;
}

}
}
