#include "cfg/ubx_cfg_sbas_parser.hpp"

namespace ublox
{
namespace cfg
{

bool SbasParser::readBuffer( const buffer_t & buffer, SbasData * data ) const
{
    data->mode = bufferToX1( HEADER_SIZE , &buffer );
    data->usage = bufferToX1( HEADER_SIZE+1 , &buffer );
    data->maxSbas = bufferToChar( HEADER_SIZE+2 , &buffer );
    data->scanmode2 = bufferToX1( HEADER_SIZE+3 , &buffer );
    data->scanmode1 = bufferToX4( HEADER_SIZE+4 , &buffer );

    return true;
}

buffer_t SbasParser::getConfigurationMsg( const SbasData * config )
{
    buffer_t buf( HEADER_SIZE + 8 + CRC_SIZE ,char( 0 ) ) ;
    buf[0] = SYNC1 ;
    buf[1] = SYNC2 ;
    buf[2] = CFG ;
    buf[3] = SBAS ;
    UShortToBuffer( 8 , 4 , &buf );
    X1ToBuffer( config->mode , HEADER_SIZE , &buf );
    X1ToBuffer( config->usage , HEADER_SIZE+1 , &buf );
    UCharToBuffer( config->maxSbas , HEADER_SIZE+2 , &buf ) ;
    X1ToBuffer( config->scanmode2 , HEADER_SIZE+3 , &buf );
    X4ToBuffer( config->scanmode1 , HEADER_SIZE+4 , &buf );

    computeCRC( buf[14] , buf[15] , &buf );
    return buf ;
}

} // end namespace cfg
} // end namespace ublox
