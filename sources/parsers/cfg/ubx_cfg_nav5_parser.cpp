#include "cfg/ubx_cfg_nav5_parser.hpp"

namespace ublox
{
namespace cfg
{

bool Nav5Parser::readBuffer( const buffer_t & buffer, Nav5Data * data ) const
{
    data->mask = bufferToX2( HEADER_SIZE,&buffer ) ;

    unsigned char d = bufferToUChar( HEADER_SIZE+2,&buffer ) ;

    if( d == 0 ) data->dynModel = PORTABLE ;
    else if( d == 2 ) data->dynModel = STATIONARY ;
    else if( d == 3 ) data->dynModel = PEDESTRIAN ;
    else if( d == 4 ) data->dynModel = AUTOMOTIVE ;
    else if( d == 5 ) data->dynModel = SEA ;
    else if( d == 6 ) data->dynModel = AIRBORNE_1G ;
    else if( d == 7 ) data->dynModel = AIRBORNE_2G ;
    else if( d == 8 ) data->dynModel = AIRBORNE_4G ;

    U1 f = bufferToUChar( HEADER_SIZE+3,&buffer ) ;

    if( f == 1 ) data->fixMode = TWOD_ONLY ;
    else if( f == 2 ) data->fixMode = THREED_ONLY ;
    else if( f == 3 ) data->fixMode = AUTO ;

    data->fixedAlt = bufferToLong( HEADER_SIZE+4,&buffer ) ;
    data->fixedAltVar = bufferToULong( HEADER_SIZE+8,&buffer ) ;
    data->minElev = bufferToChar( HEADER_SIZE+12,&buffer ) ;
    data->drLimit = bufferToUChar( HEADER_SIZE+13,&buffer ) ;
    data->pDop = bufferToUShort( HEADER_SIZE+14,&buffer );
    data->tDop = bufferToUShort( HEADER_SIZE+16,&buffer ) ;
    data->pAcc = bufferToUShort( HEADER_SIZE+18,&buffer ) ;
    data->tAcc = bufferToUShort( HEADER_SIZE+20,&buffer ) ;
    data->staticHoldThres = bufferToUChar( HEADER_SIZE+22,&buffer ) ;
    data->dgpsTimeOut = bufferToUChar( HEADER_SIZE+23,&buffer );

    return true;
}

buffer_t Nav5Parser::getConfigurationMsg( const Nav5Data * config )
{
    buffer_t buf( HEADER_SIZE+36+CRC_SIZE , char( 0 ) ) ;
    buf[0] = SYNC1 ;
    buf[1] = SYNC2 ;
    buf[2] = CFG ;
    buf[3] = NAV5 ;
    UShortToBuffer( 36 , 4 , &buf );
    UShortToBuffer( config->mask.to_ulong() , HEADER_SIZE , &buf );
    buf[HEADER_SIZE+2] = config->dynModel ;
    buf[HEADER_SIZE+3] = config->fixMode ;
    ULongToBuffer( config->fixedAlt,HEADER_SIZE+4,&buf ) ;
    ULongToBuffer( config->fixedAltVar,HEADER_SIZE+8,&buf ) ;
    buf[HEADER_SIZE+12] = config->minElev ;
    buf[HEADER_SIZE+13] = config->drLimit ;
    ULongToBuffer( config->pDop,HEADER_SIZE+14,&buf ) ;
    ULongToBuffer( config->tDop,HEADER_SIZE+16,&buf ) ;
    ULongToBuffer( config->pAcc,HEADER_SIZE+18,&buf ) ;
    ULongToBuffer( config->tAcc,HEADER_SIZE+20,&buf ) ;
    UCharToBuffer( config->staticHoldThres , HEADER_SIZE+22 , &buf );
    UCharToBuffer( config->dgpsTimeOut , HEADER_SIZE+23 , &buf );
    computeCRC( buf[42] , buf[43] , &buf );
    return buf ;
}

}
}
