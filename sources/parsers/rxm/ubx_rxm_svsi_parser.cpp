#include "rxm/ubx_rxm_svsi_parser.hpp"

namespace ublox
{
namespace rxm
{

bool SvsiParser::readBuffer( const buffer_t & buffer, SvsiData * data ) const
{
    data->iTow = bufferToLong( HEADER_SIZE,&buffer );
    data->week = bufferToUShort( HEADER_SIZE+4,&buffer );
    data->numVis = bufferToUChar( HEADER_SIZE+6,&buffer );
    data->numSV = bufferToUChar( HEADER_SIZE+7,&buffer );

    data->svInfos.clear();
    data->svInfos.resize(data->numSV);
    for( int i = 0 ; i < data->numSV ; ++i )
    {
        int j = HEADER_SIZE + 8 + 6 * i ;
        data->svInfos[i].svid   = bufferToUChar( j,&buffer );
        data->svInfos[i].svFlag = bufferToUChar( j+1,&buffer );
        data->svInfos[i].azim   = bufferToShort( j+2,&buffer ) ;
        data->svInfos[i].elev   = bufferToChar( j+4,&buffer ) ;
        data->svInfos[i].age    = bufferToUChar( j+5,&buffer ) ;
    }

    return true;
}

}
}
