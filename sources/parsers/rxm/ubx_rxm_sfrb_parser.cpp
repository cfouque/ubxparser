#include "rxm/ubx_rxm_sfrb_parser.hpp"
#include <iterator>

namespace ublox
{
namespace rxm
{

bool SfrbParser::readBuffer( const buffer_t & buffer, SfrbData * data ) const
{
    SfrbData::Block bdata ;
    bdata.chn = bufferToUChar( HEADER_SIZE , &buffer ) ;
    bdata.svId = bufferToUChar( HEADER_SIZE+1 , &buffer ) ;

    for( std::size_t k = 0 ; k < 10 ; ++k )
    {
        bdata.dwrd[k] =  bufferToLong( HEADER_SIZE+2+4*k , &buffer ) ;
    }

    if( ( bdata.svId==120 ) || ( bdata.svId==122 ) || ( bdata.svId==124 )
            || ( bdata.svId==126 ) || ( bdata.svId==129 ) ||( bdata.svId==131 )
            || ( bdata.svId==134 ) || ( bdata.svId==137 ) )
    {
        this->assignSbasBuffer( &bdata );
    }

    if( data->dwrds.size() > MAX_NUMBER_SFRB_BLOCKS )
        data->dwrds.erase( std::begin(data->dwrds) ); // pop_front

    data->dwrds.push_back( bdata );

    return true;
}

void SfrbParser::assignSbasBuffer( SfrbData::Block * block ) const
{
    block->sbas_buffer.clear() ;
    block->sbas_buffer.reserve( 32 );

    unsigned char * pTmp ;
    signed long tSl ;

    for( std::size_t i=0; i<7; ++i )
    {
        pTmp = ( unsigned char * )( & block->dwrd[i] );

        for( int j=3; j>=0; --j )
            block->sbas_buffer.push_back( pTmp[j] );
    }

    /* WORD 8 after reading from little indian

     31 ... 27 26 25 24 23 22 ... 01 00
    ____________________________________
    |XXXXXXXXXXXX|     |   PArity      |
    ----------------^-------------------
                    |
                    Last two bits
    */
    tSl = block->dwrd[7];
    tSl = tSl << 6;
    pTmp = ( unsigned char * )( & tSl );

    for( int j=3; j >= 0; --j )
        block->sbas_buffer.push_back( pTmp[j] );
}

} // end namespace rxm
} // end namespace ublox
