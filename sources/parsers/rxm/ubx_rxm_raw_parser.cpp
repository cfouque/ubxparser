#include "rxm/ubx_rxm_raw_parser.hpp"

namespace ublox
{
namespace rxm
{

bool RawParser::readBuffer( const buffer_t & buffer, RawData * data ) const
{
    data->iTow = bufferToLong( HEADER_SIZE,&buffer );
    data->week = bufferToUShort( HEADER_SIZE+4,&buffer );
    data->numSV = bufferToUChar( HEADER_SIZE+6,&buffer );

    data->meas.clear();
    data->meas.resize( data->numSV );

    for( int n = 0 ; n < data->numSV ; ++n )
    {
        int i1 = HEADER_SIZE + 8 + 24 * n ;
        data->meas[n].L1 = bufferToDouble( i1 , &buffer ) ;
        data->meas[n].C1 = bufferToDouble( i1+8 , &buffer ) ;
        data->meas[n].D1 = bufferToFloat( i1+16 , &buffer ) ;
        data->meas[n].svId = bufferToUChar( i1+20 , &buffer ) ;
        data->meas[n].QualIndic = buffer.at( i1+21 ) ;
        data->meas[n].snr = buffer.at( i1+22 ) ;
        data->meas[n].LLI = bufferToUChar( i1+23 , &buffer ) ;
    }

    return true;
}

}
}
