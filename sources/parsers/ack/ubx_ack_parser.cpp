#include "ack/ubx_ack_parser.hpp"

namespace ublox
{
namespace ack
{

bool Parser::readBuffer( const buffer_t & buffer , AckData * data ) const
{
    const char msgId = buffer.at( 3 ) ;

    if( msgId == ack::ACK )
        data->acknowledged = true ;
    else if( msgId == ack::NAK )
        data->acknowledged = false ;
    else return false ;

    data->clsID = bufferToChar( HEADER_SIZE, &buffer );
    data->msgID = bufferToChar( HEADER_SIZE+1, &buffer );

    return true;
}

} // end namespace ack
} // end namespace ublox

