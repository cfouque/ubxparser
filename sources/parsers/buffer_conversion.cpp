#include "buffer_conversion.hpp"
#include <endian.h>

//! Returns the buffer value as an ublox::U1
ublox::U1 bufferToUChar( int startByte , std::shared_ptr<ublox::buffer_t> pbuffer )
{
    return static_cast<ublox::U1>( pbuffer->at( startByte ) );
}

ublox::U1 bufferToUChar( int startByte , ublox::buffer_t * pbuffer )
{
    return static_cast<ublox::U1>( pbuffer->at( startByte ) );
}

//! Returns the buffer value as a ublox::I1
ublox::I1 bufferToChar( int startByte , std::shared_ptr<ublox::buffer_t> pbuffer )
{
    return pbuffer->at( startByte );
}

ublox::I1 bufferToChar( int startByte , ublox::buffer_t * pbuffer )
{
    return pbuffer->at( startByte );
}

//! Return the buffer value as as Unsigned short integer (int8)
ublox::U2 bufferToUShort( int startByte , std::shared_ptr<ublox::buffer_t> pbuffer )
{
    ublox::I1 tab2[2];
    ublox::U2 * pushort;
#if ___BYTE_ORDER == __BIG_ENDIAN

    for( int i = 0 ; i < 2 ; ++i ) tab2[2-i]=pbuffer->at( startByte+i );

#else

    for( int i = 0 ; i < 2 ; ++i ) tab2[i]=pbuffer->at( startByte+i );

#endif
//   for (int i=0; i<2; ++i) tab2[i]=pbuffer->at(startByte+i);
//   *(tab2) = qFromLittleEndian(*(tab2));
    pushort = reinterpret_cast<ublox::U2 *>( tab2 );
    return *pushort;
}

ublox::U2 bufferToUShort( int startByte , ublox::buffer_t * pbuffer )
{
    ublox::I1 tab2[2];
    ublox::U2 * pushort;
#if ___BYTE_ORDER == __BIG_ENDIAN

    for( int i = 0 ; i < 2 ; ++i ) tab2[2-i]=pbuffer->at( startByte+i );

#else

    for( int i = 0 ; i < 2 ; ++i ) tab2[i]=pbuffer->at( startByte+i );

#endif
//   for (int i=0; i<2; ++i) tab2[i]=pbuffer->at(startByte+i);
//   *(tab2) = qFromLittleEndian(*(tab2));
    pushort = reinterpret_cast<ublox::U2 *>( tab2 );
    return *pushort;
}

//! Return the buffer value as as Unsigned short integer (int8)
short bufferToShort( int startByte , std::shared_ptr<ublox::buffer_t> pbuffer )
{
    ublox::I1 tab2[2];
    short * pshort;
#if ___BYTE_ORDER == __BIG_ENDIAN

    for( int i = 0 ; i < 2 ; ++i ) tab2[2-i]=pbuffer->at( startByte+i );

#else

    for( int i = 0 ; i < 2 ; ++i ) tab2[i]=pbuffer->at( startByte+i );

#endif
//   for (int i=0; i<2; ++i) tab2[i]=pbuffer->at(startByte+i);
//   *(tab2) = qFromLittleEndian(*(tab2));
    pshort = reinterpret_cast<short *>( tab2 );
    return *pshort;
}

short bufferToShort( int startByte , ublox::buffer_t * pbuffer )
{
    ublox::I1 tab2[2];
    short * pshort;
#if ___BYTE_ORDER == __BIG_ENDIAN

    for( int i = 0 ; i < 2 ; ++i ) tab2[2-i]=pbuffer->at( startByte+i );

#else

    for( int i = 0 ; i < 2 ; ++i ) tab2[i]=pbuffer->at( startByte+i );

#endif
//   for (int i=0; i<2; ++i) tab2[i]=pbuffer->at(startByte+i);
//   *(tab2) = qFromLittleEndian(*(tab2));
    pshort = reinterpret_cast<short *>( tab2 );
    return *pshort;
}

//! Returns the bufer as an Unsigned Long Integer (uint16) ;
ublox::U4 bufferToULong( int startByte, std::shared_ptr<ublox::buffer_t> pbuffer )
{
    ublox::I1 tab4[4];
    ublox::U4 * pULong;
#if ___BYTE_ORDER == __BIG_ENDIAN

    for( int i = 0 ; i < 4 ; ++i ) tab4[4-i]=pbuffer->at( startByte+i );

#else

    for( int i = 0 ; i < 4 ; ++i ) tab4[i]=pbuffer->at( startByte+i );

#endif
//   for (int i=0; i<4; ++i) tab4[i]=pbuffer->at(startByte+i);
//   *(tab4) = qFromLittleEndian(*(tab4));
    pULong = reinterpret_cast<ublox::U4 *>( tab4 );
    return *pULong;
}

ublox::U4 bufferToULong( int startByte, ublox::buffer_t * pbuffer )
{
    ublox::I1 tab4[4];
    ublox::U4 * pULong;
#if ___BYTE_ORDER == __BIG_ENDIAN

    for( int i = 0 ; i < 4 ; ++i ) tab4[4-i]=pbuffer->at( startByte+i );

#else

    for( int i = 0 ; i < 4 ; ++i ) tab4[i]=pbuffer->at( startByte+i );

#endif
//   for (int i=0; i<4; ++i) tab4[i]=pbuffer->at(startByte+i);
//   *(tab4) = qFromLittleEndian(*(tab4));
    pULong = reinterpret_cast<ublox::U4 *>( tab4 );
    return *pULong;
}

//! Returns the buffer value as an Signed long integer (int16)
ublox::I4 bufferToLong( int startByte, std::shared_ptr<ublox::buffer_t> pbuffer )
{
    ublox::I1 tab4[4];
    ublox::I4 * plong;
#if ___BYTE_ORDER == __BIG_ENDIAN

    for( int i = 0 ; i < 4 ; ++i )
        tab4[4-i]=pbuffer->at( startByte+i );

#else

    for( int i = 0 ; i < 4 ; ++i )
        tab4[i]=pbuffer->at( startByte+i );

#endif
    plong = reinterpret_cast<ublox::I4 *>( tab4 );
    return *plong;
}

ublox::I4 bufferToLong( int startByte, const ublox::buffer_t * pbuffer )
{
    ublox::I1 tab4[4];
    ublox::I4 * plong;
#if ___BYTE_ORDER == __BIG_ENDIAN

    for( int i = 0 ; i < 4 ; ++i )
        tab4[4-i]=pbuffer->at( startByte+i );

#else

    for( int i = 0 ; i < 4 ; ++i )
        tab4[i]=pbuffer->at( startByte+i );

#endif
//   for (int i=0; i<4; ++i) tab4[i]=pbuffer->at(startByte+i);
//   *(tab4) = qFromLittleEndian(*(tab4));
    plong = reinterpret_cast<ublox::I4 *>( tab4 );
    return *plong;
}

//! returns the buffer value as a ublox::R4
ublox::R4 bufferToFloat( int startByte, std::shared_ptr<ublox::buffer_t> pbuffer )
{
    ublox::I1 tab8[4];
    ublox::R4 * pdouble;
#if ___BYTE_ORDER == __BIG_ENDIAN

    for( int i=0; i<4; ++i ) tab8[4-i]=pbuffer->at( startByte+i );

#else

    for( int i=0; i<4; ++i ) tab8[i]=pbuffer->at( startByte+i );

#endif
//   for (int i=0; i<4; ++i) tab8[i]=pbuffer->at(startByte+i);
//   *(tab8) = qFromLittleEndian(*(tab8));
    pdouble = reinterpret_cast<ublox::R4 *>( tab8 );
    return *pdouble;
}

ublox::R4 bufferToFloat( int startByte, ublox::buffer_t * pbuffer )
{
    ublox::I1 tab8[4];
    ublox::R4 * pdouble;
#if ___BYTE_ORDER == __BIG_ENDIAN

    for( int i=0; i<4; ++i )
        tab8[4-i]=pbuffer->at( startByte+i );

#else

    for( int i=0; i<4; ++i )
        tab8[i]=pbuffer->at( startByte+i );

#endif
    pdouble = reinterpret_cast<ublox::R4 *>( tab8 );
    return *pdouble;
}

//! returns the buffer value as a double
ublox::R8 bufferToDouble( int startByte, std::shared_ptr<ublox::buffer_t> pbuffer )
{
    ublox::I1 tab8[8];
    ublox::R8 * pdouble;
#if ___BYTE_ORDER == __BIG_ENDIAN

    for( int i=0; i<8; ++i )
        tab8[8-i]=pbuffer->at( startByte+i );

#else

    for( int i=0; i<8; ++i )
        tab8[i]=pbuffer->at( startByte+i );

#endif
    pdouble = reinterpret_cast<ublox::R8 *>( tab8 );
    return *pdouble;
}

ublox::R8 bufferToDouble( int startByte, ublox::buffer_t * pbuffer )
{
    ublox::I1 tab8[8];
    ublox::R8 * pdouble;
#if ___BYTE_ORDER == __BIG_ENDIAN

    for( int i=0; i<8; ++i )
        tab8[8-i]=pbuffer->at( startByte+i );

#else

    for( int i=0; i<8; ++i )
        tab8[i]=pbuffer->at( startByte+i );

#endif
    pdouble = reinterpret_cast<double *>( tab8 );
    return *pdouble;
}

//! extract the value of byte at pos iiin a ublox::I1.
bool extractBit( ublox::I1 byte, int pos )
{
    return ( byte >> pos ) & 0x01;
}

ublox::X1 bufferToX1( int startByte , std::shared_ptr<ublox::buffer_t>  pbuffer )
{
    ublox::X1 flag ;

    for( int k = 0 ; k < 8 ; ++k ) flag[k] = extractBit( pbuffer->at( startByte ) , k ) ;

    return flag ;
}

ublox::X1 bufferToX1( int startByte , ublox::buffer_t * pbuffer )
{
    ublox::X1 flag ;

    for( int k = 0 ; k < 8 ; ++k ) flag[k] = extractBit( pbuffer->at( startByte ) , k ) ;

    return flag ;
}


ublox::X2 bufferToX2( int startByte , std::shared_ptr<ublox::buffer_t>  pbuffer )
{
    ublox::X2 flag ;
    int n = startByte ;

    for( int k = 0 ; k < 16 ; ++k )
    {
        if( k == 8 ) n++ ;

        flag[k] = extractBit( pbuffer->at( n ) , k ) ;
    }

    return flag ;
}

ublox::X2 bufferToX2( int startByte , ublox::buffer_t * pbuffer )
{
    ublox::X2 flag ;
    int n = startByte ;

    for( int k = 0 ; k < 16 ; ++k )
    {
        if( k == 8 ) n++ ;

        flag[k] = extractBit( pbuffer->at( n ) , k ) ;
    }

    return flag ;
}

ublox::X4 bufferToX4( int startByte , std::shared_ptr<ublox::buffer_t>  pbuffer )
{
    ublox::X4 flag ;
    int n = startByte ;

    for( int k = 0 ; k < 32 ; ++k )
    {
        if( k == 8 ) n++ ;

        flag[k] = extractBit( pbuffer->at( n ) , k ) ;
    }

    return flag ;
}

ublox::X4 bufferToX4( int startByte , ublox::buffer_t * pbuffer )
{
    ublox::X4 flag ;
    int n = startByte ;

    for( int k = 0 ; k < 32 ; ++k )
    {
        if( k == 8 ) n++ ;

        flag[k] = extractBit( pbuffer->at( n ) , k ) ;
    }

    return flag ;
}

// ublox::X4 bufferToX4( int startByte , ublox::buffer_t * pbuffer );


/*!
 *
 */
ublox::U1 extractSFID( const long * DWRD )
{
    return ( ublox::U1 )( ( ( DWRD[1] ) &0x0000001C ) >>2 );
//     return (ublox::U1)(((DWRD[1])&0x00000700)>>8);
}

/*!
 *
 */
ublox::U2 extractTOW( const long * DWRD )
{
    return ( ublox::U2 )( ( ( DWRD[1] ) &0xFFFF80 ) >>7 );
}

/*!
 *
 */
ublox::U2 extractSVID( const long * DWRD )
{
    return ( ublox::U2 )( ( ( DWRD[2] ) &0x1F8000 ) >>15 );
}

void UShortToBuffer( ublox::U2 val , unsigned int startByte , ublox::buffer_t * pbuffer )
{
    pbuffer->at( startByte ) = val & 0xff ;
    pbuffer->at( startByte+1 ) = ( val >> 8 ) & 0xff ;
}

void ShortToBuffer( ublox::I2 val , uint startByte , ublox::buffer_t * pbuffer )
{
    pbuffer->at( startByte ) = val & 0xff ;
    pbuffer->at( startByte+1 ) = ( val >> 8 ) & 0xff ;
}

void ULongToBuffer( ublox::U4 val , uint startByte , ublox::buffer_t * pbuffer )
{
    pbuffer->at( startByte ) = val & 0xff ;
    pbuffer->at( startByte+1 ) = ( val >> 8 ) & 0xff ;
    pbuffer->at( startByte+2 ) = ( val >> 16 ) & 0xff ;
    pbuffer->at( startByte+3 ) = ( val >> 24 ) & 0xff ;
}

void LongToBuffer( ublox::I4 val , uint startByte , ublox::buffer_t * pbuffer )
{
    pbuffer->at( startByte ) = val & 0xff ;
    pbuffer->at( startByte+1 ) = ( val >> 8 ) & 0xff ;
    pbuffer->at( startByte+2 ) = ( val >> 16 ) & 0xff ;
    pbuffer->at( startByte+3 ) = ( val >> 24 ) & 0xff ;

}


void FloatToBuffer( ublox::R4 val , uint startByte , ublox::buffer_t * pbuffer )
{
    std::stringstream sstream ;
    sstream << val ;
    std::string c_str = sstream.str() ;

    for( uint k = 0 ; k < 4 ; ++k )
        pbuffer->at( startByte+k ) = c_str.at( k ) ;
}

void DoubleToBuffer( double val , uint startByte , ublox::buffer_t * pbuffer )
{
    std::stringstream sstream ;
    sstream << val ;
    std::string c_str = sstream.str() ;

    for( uint k = 0 ; k < 8 ; ++k )
        pbuffer->at( startByte+k ) = c_str.at( k ) ;
}

void X1ToBuffer( ublox::X1 val , uint startByte , ublox::buffer_t * pbuffer )
{
    unsigned int v = val.to_ulong() ;
    pbuffer->at( startByte ) = v & 0xff ;
}

void X2ToBuffer( ublox::X2 val , uint startByte , ublox::buffer_t * pbuffer )
{
    unsigned int v = val.to_ulong() ;
    pbuffer->at( startByte ) = v & 0xff ;
    pbuffer->at( startByte+1 ) = ( v >> 8 ) & 0xff ;
}

void X4ToBuffer( ublox::X4 val , uint startByte , ublox::buffer_t * pbuffer )
{
    ULongToBuffer( val.to_ulong() , startByte , pbuffer );
}

void UCharToBuffer( ublox::U1 val , unsigned int startByte , ublox::buffer_t * p_buffer )
{
    p_buffer->at( startByte ) = static_cast<ublox::I1>( val ) ;
}
