#include "nav/ubx_nav_posecef_parser.hpp"

namespace ublox
{
namespace nav
{

bool PosecefParser::readBuffer( const buffer_t & buffer, PosecefData * data ) const
{
    data->iTow = bufferToULong( HEADER_SIZE,&buffer );
    data->Xecef = bufferToLong( HEADER_SIZE+4,&buffer );
    data->Yecef = bufferToLong( HEADER_SIZE+8,&buffer );
    data->Zecef = bufferToLong( HEADER_SIZE+12,&buffer );
    data->pAcc = bufferToULong( HEADER_SIZE+16,&buffer );

    return true;
}

}
}
