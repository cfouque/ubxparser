#include "nav/ubx_nav_sbas_parser.hpp"

namespace ublox
{
namespace nav
{

bool SbasParser::readBuffer( const buffer_t & buffer, SbasData * data ) const
{
    data->iTow = bufferToULong( HEADER_SIZE , &buffer );
    data->geo = bufferToUChar( HEADER_SIZE+4 , &buffer );
    data->mode = bufferToUChar( HEADER_SIZE+5 , &buffer );
    data->sys = bufferToChar( HEADER_SIZE+6 , &buffer );

    char flags = bufferToChar( HEADER_SIZE+7 , &buffer );

    for( int k = 0 ; k < 4 ; ++k )
    {
        data->service[k] = extractBit( flags,k ) ;
    }

    data->cnt = bufferToUChar( HEADER_SIZE+8 , &buffer );
    data->sv.clear();
    data->sv.resize( data->cnt );

    for( int k = 0 ; k < data->cnt ; ++k )
    {
        int i1 = HEADER_SIZE + 12 + 12 * k ;
        data->sv[k].svid = bufferToUChar( i1 , &buffer );
        data->sv[k].flags = bufferToUChar( i1+1 , &buffer );
        data->sv[k].udre = bufferToUChar( i1+2 , &buffer );
        data->sv[k].svSys = bufferToUChar( i1+3 , &buffer );
        char v = bufferToChar( i1+4 , &buffer );

        for( int k = 0 ; k < 4 ; ++k )
            data->sv[k].svService[k] = extractBit( v,k );

        data->sv[k].prc = bufferToShort( i1+6 , &buffer );
        data->sv[k].ic = bufferToShort( i1+10 , &buffer );
    }

    return true;
}

}
}
