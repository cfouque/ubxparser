#include "nav/ubx_nav_dgps_parser.hpp"

namespace ublox
{
namespace nav
{

bool DgpsParser::readBuffer( const buffer_t & buffer, DgpsData * data ) const
{
    data->iTow = bufferToULong( HEADER_SIZE,&buffer );
    data->age = bufferToLong( HEADER_SIZE+4,&buffer );
    data->baseId = bufferToShort( HEADER_SIZE+8,&buffer );
    data->baseHealth = bufferToShort( HEADER_SIZE+10,&buffer );
    data->numCh = bufferToUChar( HEADER_SIZE+12,&buffer );
    data->status = bufferToUChar( HEADER_SIZE+13,&buffer );

    data->chData.clear();
    data->chData.resize( data->numCh );
    for( int k = 0 ; k < data->numCh ; ++k )
    {
        int i1 = HEADER_SIZE+12*k ;
        data->chData[k].svid = bufferToUChar( i1+16 , &buffer );
        data->chData[k].flags = bufferToUChar( i1+17 , &buffer ) ;
        data->chData[k].ageC = bufferToUShort( i1+18 , &buffer );
        data->chData[k].prc = bufferToFloat( i1+20 , &buffer );
        data->chData[k].prrc = bufferToFloat( i1+24 , &buffer );
    }

    return true;
}

}
}
