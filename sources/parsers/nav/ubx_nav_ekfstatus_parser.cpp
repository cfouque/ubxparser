#include "nav/ubx_nav_ekfstatus_parser.hpp"

namespace ublox
{
namespace nav
{

bool EkfstatusParser::readBuffer( const buffer_t & buffer, EkfstatusData * data ) const
{
    data->pulses = bufferToLong( HEADER_SIZE,&buffer ) ;
    data->period = bufferToLong( HEADER_SIZE+4,&buffer ) ;
    data->gyroMean = bufferToULong( HEADER_SIZE+8,&buffer ) ;
    data->temperature = bufferToShort( HEADER_SIZE+12,&buffer ) ;
    data->direction = bufferToChar( HEADER_SIZE+14,&buffer ) ;
    data->calibStatus = bufferToX1( HEADER_SIZE+15,&buffer ) ;
    data->pulseScale = bufferToLong( HEADER_SIZE+16,&buffer );
    data->gyroBias = bufferToLong( HEADER_SIZE+20,&buffer );
    data->gyroScale = bufferToLong( HEADER_SIZE+24,&buffer ) ;
    data->accPulseScale= bufferToShort( HEADER_SIZE+28,&buffer );
    data->accGyroBias = bufferToShort( HEADER_SIZE+30,&buffer ) ;
    data->accGyroScale= bufferToShort( HEADER_SIZE+32,&buffer ) ;
    data->measUsed = bufferToX1( HEADER_SIZE+34,&buffer ) ;

    return true;
}

}
}
