#include "nav/ubx_nav_posllh_parser.hpp"

namespace ublox
{
namespace nav
{

bool PosllhParser::readBuffer( const buffer_t & buffer, PosllhData * data ) const
{
    data->iTow = bufferToULong( HEADER_SIZE,&buffer );
    data->longitude = bufferToLong( HEADER_SIZE+4,&buffer ) ;
    data->latitude = bufferToLong( HEADER_SIZE+8,&buffer ) ;
    data->hElli = bufferToLong( HEADER_SIZE+12 , &buffer );
    data->hMsl = bufferToLong( HEADER_SIZE+16 , &buffer );
    data->hAcc = bufferToULong( HEADER_SIZE+20 , &buffer );
    data->vAcc = bufferToLong( HEADER_SIZE+24 , &buffer );

    return true;
}

}
}
