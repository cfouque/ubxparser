#include "nav/ubx_nav_status_parser.hpp"

namespace ublox
{
namespace nav
{

bool StatusParser::readBuffer( const buffer_t & buffer, StatusData * data ) const
{
    data->iTow = bufferToULong( HEADER_SIZE,&buffer );
    data->gpsFix = bufferToUChar( HEADER_SIZE+4,&buffer ) ;
    data->flags = bufferToX1( HEADER_SIZE+5,&buffer ) ;
    data->fixStat = bufferToX1( HEADER_SIZE+6,&buffer ) ;
    data->flags2 = bufferToX1( HEADER_SIZE+7,&buffer ) ;
    data->ttff = bufferToULong( HEADER_SIZE+8,&buffer );
    data->msss = bufferToULong( HEADER_SIZE+12,&buffer ) ;

    return true;
}

}
}
