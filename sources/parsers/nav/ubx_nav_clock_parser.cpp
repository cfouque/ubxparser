#include "nav/ubx_nav_clock_parser.hpp"

namespace ublox
{
namespace nav
{

bool ClockParser::readBuffer( const buffer_t & buffer, ClockData * data ) const
{
    data->iTow = bufferToULong( HEADER_SIZE,&buffer );
    data->clkB = bufferToLong( HEADER_SIZE+4 , &buffer ) ;
    data->clkD = bufferToLong( HEADER_SIZE+8 , &buffer );
    data->tAcc = bufferToULong( HEADER_SIZE+12 , &buffer );
    data->fAcc = bufferToULong( HEADER_SIZE+16 , &buffer );

    return true;
}

}
}
