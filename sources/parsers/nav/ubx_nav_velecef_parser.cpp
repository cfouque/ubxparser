#include "nav/ubx_nav_velecef_parser.hpp"

namespace ublox
{
namespace nav
{

bool VelecefParser::readBuffer( const buffer_t & buffer, VelecefData * data ) const
{
    data->iTow = bufferToULong( HEADER_SIZE , &buffer );
    data->vXecef = bufferToLong( HEADER_SIZE+4 , &buffer );
    data->vYecef = bufferToLong( HEADER_SIZE+8 , &buffer );
    data->vZecef = bufferToLong( HEADER_SIZE+12 , &buffer );
    data->sAcc = bufferToULong( HEADER_SIZE+16 , &buffer );

    return true;
}

}
}
