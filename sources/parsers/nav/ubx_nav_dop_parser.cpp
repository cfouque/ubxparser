#include "nav/ubx_nav_dop_parser.hpp"

namespace ublox
{
namespace nav
{

bool DopParser::readBuffer( const buffer_t & buffer, DopData * data ) const
{
    data->iTow = bufferToULong( HEADER_SIZE,&buffer );
    data->gDOP = bufferToUShort( HEADER_SIZE+4,&buffer );
    data->pDOP = bufferToUShort( HEADER_SIZE+6,&buffer );
    data->tDOP = bufferToUShort( HEADER_SIZE+8,&buffer );
    data->vDOP = bufferToUShort( HEADER_SIZE+10,&buffer );
    data->hDOP = bufferToUShort( HEADER_SIZE+12,&buffer );
    data->nDOP = bufferToUShort( HEADER_SIZE+14,&buffer );
    data->eDOP = bufferToUShort( HEADER_SIZE+16,&buffer );

    return true;
}

}
}
