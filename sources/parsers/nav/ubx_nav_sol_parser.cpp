#include "nav/ubx_nav_sol_parser.hpp"

namespace ublox
{
namespace nav
{

bool SolParser::readBuffer( const buffer_t & buffer, SolData * data ) const
{
    data->iTow = bufferToULong( HEADER_SIZE,&buffer );
    data->week = bufferToUShort( HEADER_SIZE+8,&buffer );
    data->fTow = bufferToLong( HEADER_SIZE+4,&buffer );
    data->gpsFix = bufferToUChar( HEADER_SIZE+10,&buffer );

    char f = bufferToChar( HEADER_SIZE+11,&buffer );
    for( unsigned int k = 0 ; k < data->flags.size() ; ++k )
    {
        data->flags[k] = extractBit( f,k );
    }

    data->Xecef = bufferToLong( HEADER_SIZE+12,&buffer );
    data->Yecef = bufferToLong( HEADER_SIZE+16,&buffer );
    data->Zecef = bufferToLong( HEADER_SIZE+20,&buffer );
    data->pAcc = bufferToULong( HEADER_SIZE+24,&buffer );
    data->vXecef = bufferToLong( HEADER_SIZE+28,&buffer );
    data->vYecef = bufferToLong( HEADER_SIZE+32,&buffer );
    data->vZecef = bufferToLong( HEADER_SIZE+36,&buffer );
    data->sAcc = bufferToULong( HEADER_SIZE+40,&buffer );
    data->pdop = bufferToUShort( HEADER_SIZE+44,&buffer );
    data->numSV = bufferToUChar( HEADER_SIZE+47,&buffer );

    return true;
}

}
}
