#include "nav/ubx_nav_aopstatus_parser.hpp"

namespace ublox
{
namespace nav
{

bool AopstatusParser::readBuffer( const buffer_t & buffer, AopstatusData * data ) const
{
    data->iTow = bufferToULong( HEADER_SIZE,&buffer );
    data->config = bufferToChar( HEADER_SIZE+4,&buffer );
    data->status = bufferToChar( HEADER_SIZE+5,&buffer );
    data->avail = bufferToULong( HEADER_SIZE+8,&buffer ) ;

    return true;
}

}
}
