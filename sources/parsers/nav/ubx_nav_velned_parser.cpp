#include "nav/ubx_nav_velned_parser.hpp"

namespace ublox
{
namespace nav
{

bool VelnedParser::readBuffer( const buffer_t & buffer, VelnedData * data ) const
{
    data->iTow = bufferToULong( HEADER_SIZE,&buffer );
    data->velN = bufferToLong( HEADER_SIZE+4 , &buffer ) ;
    data->velE = bufferToLong( HEADER_SIZE+8 , &buffer );
    data->velD = bufferToLong( HEADER_SIZE+12 , &buffer );
    data->speed = bufferToULong( HEADER_SIZE+16 , &buffer );
    data->gSpeed = bufferToULong( HEADER_SIZE+20 , &buffer );
    data->heading = bufferToLong( HEADER_SIZE+24 , &buffer );
    data->sAcc = bufferToULong( HEADER_SIZE+28 , &buffer );
    data->cAcc = bufferToULong( HEADER_SIZE+32 , &buffer );

    return true;
}

}
}
