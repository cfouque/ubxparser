#include "nav/ubx_nav_timeutc_parser.hpp"

namespace ublox
{
namespace nav
{

bool TimeutcParser::readBuffer( const buffer_t & buffer, TimeutcData * data ) const
{
    data->iTow = bufferToULong( HEADER_SIZE,&buffer );
    data->tAcc = bufferToULong( HEADER_SIZE+4,&buffer ) ;
    data->nano = bufferToLong( HEADER_SIZE+8,&buffer ) ;
    data->year = bufferToUShort( HEADER_SIZE+12,&buffer ) ;
    data->month = bufferToUChar( HEADER_SIZE+14,&buffer ) ;
    data->day = bufferToUChar( HEADER_SIZE+15,&buffer ) ;
    data->hour = bufferToUChar( HEADER_SIZE+16,&buffer ) ;
    data->min = bufferToUChar( HEADER_SIZE+17,&buffer ) ;
    data->sec = bufferToUChar( HEADER_SIZE+18,&buffer ) ;
    data->valid = bufferToX1( HEADER_SIZE+19,&buffer ) ;

    return true;
}

}
}
