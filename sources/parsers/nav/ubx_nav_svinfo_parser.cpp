#include "nav/ubx_nav_svinfo_parser.hpp"

namespace ublox
{
namespace nav
{

bool SvinfoParser::readBuffer( const buffer_t & buffer, SvinfoData * data ) const
{
    data->iTow = bufferToULong( HEADER_SIZE , &buffer );
    data->numCh = bufferToUChar( HEADER_SIZE+4 , &buffer );
    data->globalFlags = bufferToChar( HEADER_SIZE+5 , &buffer );

    data->chData.clear();
    data->chData.resize( data->numCh );
    for( int k = 0 ; k < data->numCh ; ++k )
    {
        int i1 = HEADER_SIZE + 8 + 12 * k ;
        data->chData[k].chId = bufferToUChar( i1 , &buffer ) ;
        data->chData[k].svId = bufferToUChar( i1+1 , &buffer );
        data->chData[k].flags = bufferToX1( i1+2 , &buffer ) ;
        data->chData[k].quality = bufferToChar( i1+3 , &buffer );
        data->chData[k].cn0 = bufferToUChar( i1+4 , &buffer ) ;
        data->chData[k].elev = bufferToChar( i1+5 , &buffer ) ;
        data->chData[k].azim = bufferToShort( i1+6 , &buffer ) ;
        data->chData[k].prRes = bufferToLong( i1+8 , &buffer );
    }

    return true;
}

}
}
