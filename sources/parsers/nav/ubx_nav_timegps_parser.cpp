#include "nav/ubx_nav_timegps_parser.hpp"

namespace ublox
{
namespace nav
{

bool TimegpsParser::readBuffer( const buffer_t & buffer, TimegpsData * data ) const
{
    data->iTow = bufferToULong( HEADER_SIZE,&buffer );
    data->fTow = bufferToLong( HEADER_SIZE+4 , &buffer ) ;
    data->week = bufferToShort( HEADER_SIZE+8 , &buffer );
    data->leapS = bufferToChar( HEADER_SIZE+10 , &buffer );

    char v = bufferToChar( HEADER_SIZE+11 , &buffer );

    for( unsigned int k = 0 ; k < data->valid.size() ; ++k )
    {
        data->valid[k] = extractBit( v,k );
    }

    data->tAcc = bufferToULong( HEADER_SIZE+12 , &buffer );

    return true;
}

}
}
