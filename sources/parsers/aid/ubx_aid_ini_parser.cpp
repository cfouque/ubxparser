#include "aid/ubx_aid_ini_parser.hpp"

namespace ublox
{
namespace aid
{

bool IniParser::readBuffer ( const buffer_t & buffer, IniData * data ) const
{
    data->ecefXOrLat = bufferToLong ( HEADER_SIZE , &buffer ) ;
    data->ecefYOrLon = bufferToLong ( HEADER_SIZE+4 , &buffer ) ;
    data->ecefZOrAlt = bufferToLong ( HEADER_SIZE+8 , &buffer ) ;
    data->posAcc = bufferToULong ( HEADER_SIZE+12 , &buffer ) ;
    data->tmCfg = bufferToX2 ( HEADER_SIZE+16 , &buffer ) ;
    data->wn = bufferToUShort ( HEADER_SIZE+18 , &buffer ) ;
    data->tow = bufferToULong ( HEADER_SIZE+20 , &buffer ) ;
    data->towNs = bufferToLong ( HEADER_SIZE+24 , &buffer ) ;
    data->tAccMs = bufferToULong ( HEADER_SIZE+28 , &buffer ) ;
    data->tAccNs = bufferToULong ( HEADER_SIZE+32 , &buffer ) ;
    data->clkDOrFreq = bufferToLong ( HEADER_SIZE+36 , &buffer ) ;
    data->clkDAccOrFreqAcc = bufferToULong ( HEADER_SIZE+40 , &buffer ) ;
    data->flags = bufferToX4 ( HEADER_SIZE+44 , &buffer ) ;

    return true;
}

}
}
