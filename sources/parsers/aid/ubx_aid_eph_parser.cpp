#include "aid/ubx_aid_eph_parser.hpp"

namespace ublox
{
namespace aid
{

bool EphParser::readBuffer( const buffer_t & buffer, EphData * data ) const
{
    data->svid = bufferToULong( HEADER_SIZE , &buffer ) ;
    data->how = bufferToULong( HEADER_SIZE+4 , &buffer ) ;

    if( data->how )
    {
        for( int k = 0 ; k < 8 ; ++k )
        {
            int i0 = HEADER_SIZE + 8 + k * 4 ;
            data->sf1d[k] = bufferToULong( i0 , &buffer ) ;
            data->sf2d[k] = bufferToULong( i0+32 , &buffer ) ;
            data->sf3d[k] = bufferToULong( i0+64 , &buffer ) ;
        }
    }

    return true;
}

buffer_t EphParser::generatePollRequest( U1 svid )
{
    buffer_t buf( 9 , char( 0 ) );
    buf[0] = SYNC1 ;
    buf[1] = SYNC2 ;
    buf[2] = msgClass() ;
    buf[3] = msgId() ;
    buf[4] = 0 ;
    buf[5] = 1 ;
    buf[6] = svid ;
    computeCRC( buf[7] , buf[8] , &buf );
    return buf ;
}

} // end namespace aid
} // end namespace ublox
