#include "aid/ubx_aid_alm_parser.hpp"

namespace ublox
{
namespace aid
{

bool AlmParser::readBuffer( const buffer_t & buffer, AlmData * data ) const
{
    data->svid = bufferToULong( HEADER_SIZE , &buffer ) ;
    data->almWeek = bufferToULong( HEADER_SIZE+4 , &buffer ) ;

    if( data->almWeek )
    {
        for( int k = 0 ; k < 8 ; ++k )
        {
            int i0 = HEADER_SIZE + 8 ;
            data->dwrd[k] = bufferToULong( i0+k*4 , &buffer ) ;
        }
    }

    return true;
}

buffer_t AlmParser::generatePollRequest( U1 svid )
{
    buffer_t buf( 9 , char( 0 ) );
    buf[0] = SYNC1 ;
    buf[1] = SYNC2 ;
    buf[2] = msgClass() ;
    buf[3] = msgId() ;
    buf[4] = 0 ;
    buf[5] = 1 ;
    buf[6] = svid ;
    computeCRC( buf[7] , buf[8] , &buf );
    return buf ;
}

} // end namespace aid
} // end namespace ublox
