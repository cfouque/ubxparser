/*!
 * \file UbxAidHuiParser.hpp
 * \author Cl??ment Fouque
 * \date 27-07-2011
 * \version 2
 *
 * This file contains the declaration for ublox::aid::HuiParser
 *
 */

#include "aid/ubx_aid_hui_parser.hpp"

namespace ublox
{
namespace aid
{

bool HuiParser::readBuffer( const buffer_t & buffer, HuiData * data ) const
{
    int i0 = HEADER_SIZE ;
    data->health = bufferToX4 ( i0,&buffer );
    data->utcA[0] = bufferToDouble ( i0+4,&buffer );
    data->utcA[1] = bufferToDouble ( i0+12,&buffer );
    data->utcTOW = bufferToLong ( i0+20,&buffer );
    data->utcWNT = bufferToShort ( i0+24,&buffer );
    data->utcLS = bufferToShort ( i0+26,&buffer );
    data->utcWNF = bufferToShort ( i0+28,&buffer );
    data->utcDN = bufferToShort ( i0+30,&buffer );
    data->utcLSF = bufferToShort ( i0+32,&buffer );
    data->utcSpare = bufferToShort ( i0+34,&buffer );

    for ( int k = 0 ; k < 4 ; ++k )
    {
        data->klobA[k] = bufferToFloat ( i0+34+k*4 , &buffer ) ;
        data->klobB[k] = bufferToFloat ( i0+52+k*4 , &buffer ) ;
    }

    data->flags = bufferToX4 ( i0+68 , &buffer ) ;
    return true;
}

}
}
