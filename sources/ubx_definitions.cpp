#include "ubx_definitions.hpp"
#include "buffer_conversion.hpp"
#include "ubx_datatype.hpp"

namespace ublox
{

//! Extract MSG_ID from buffer
MSG_ID getMsgUniqueId( const buffer_t & buffer )
{
    return getMsgUniqueId( buffer[2], buffer[3] );
}

//! Extract the message size from the header
U2 messageSize( const buffer_t * pbuf )
{
    return ( payloadSize( pbuf )  + HEADER_SIZE + CRC_SIZE ) ;
}

U2 payloadSize( const buffer_t * pbuffer )
{
    return bufferToUShort( 4 , pbuffer ) ;
}

//! Compute the CRC
bool checkCRC( const buffer_t * pbuffer )
{
    buffer_v a = 0 ;
    buffer_v b = 0 ;
    computeCRC( a,b,pbuffer );

    if( ( a==pbuffer->at( pbuffer->size()-2 ) ) && ( b==pbuffer->at( pbuffer->size()-1 ) ) )
        return true;
    else
        return false ;
}

void computeCRC( buffer_v & crc1 , buffer_v & crc2 , const buffer_t * msg )
{
    crc1 = 0 ;
    crc2 = 0 ;

    for( unsigned int i=2 ; i < ( msg->size()-2 ) ; i++ )
    {
        crc1 = crc1 + msg->at( i );
        crc2 = crc2 + crc1  ;
    }
}

/*
 * shared_ptr version
 */
U2 messageSize( std::shared_ptr<buffer_t>  pbuf )
{
    return ( payloadSize( pbuf )  + HEADER_SIZE + CRC_SIZE ) ;
}

U2 payloadSize( std::shared_ptr<buffer_t>  pbuffer )
{
    return bufferToUShort( 4 , pbuffer ) ;
}

bool checkCRC( std::shared_ptr<buffer_t>  pbuffer )
{
    buffer_v a = 0 ;
    buffer_v b = 0 ;
    computeCRC( a,b,pbuffer );

//   std::cout << (int)a << " " << int(b) << " " << (int)pbuffer->at ( pbuffer->size()-2 ) << " " << (int)pbuffer->at ( pbuffer->size()-2 ) << std::endl ;

    if( ( a==pbuffer->at( pbuffer->size()-2 ) ) && ( b==pbuffer->at( pbuffer->size()-1 ) ) )
        return true;
    else
        return false ;
}

void computeCRC( buffer_v & crc1 , buffer_v & crc2 , std::shared_ptr<buffer_t>  msg )
{
    crc1 = 0 ;
    crc2 = 0 ;

    for( unsigned int i=2 ; i < ( msg->size()-2 ) ; i++ )
    {
        crc1 = crc1 + msg->at( i );
        crc2 = crc2 + crc1  ;
    }
}

I4 subframeCorrection( I4 in )
{
    I4 subframe = in ;
    subframe <<= 6 ;
    subframe &= 0x3FFFFFC0 ;
    return subframe ;
}

std::string msgIdAsString( const U1 & msg_cl , const U1 & msg_id )
{
    std::string out( "UBX " ) ;

    if( msg_cl == NAV )
    {
        out += "NAV-" ;
        using namespace nav ;

        if( msg_id == AOPSTATUS ) out += "AOPSTATUS" ;
        else if( msg_id == CLOCK ) out += "CLOCK" ;
        else if( msg_id == DGPS ) out += "DGPS" ;
        else if( msg_id == DOP ) out += "DOP" ;
        else if( msg_id == EKFSTATUS ) out += "EKFSTATUS" ;
        else if( msg_id == POSECEF ) out += "POSECEF" ;
        else if( msg_id == POSLLH ) out += "POSLLH" ;
        else if( msg_id == SBAS ) out += "SBAS" ;
        else if( msg_id == SOL ) out += "SOL" ;
        else if( msg_id == STATUS ) out += "STATUS" ;
        else if( msg_id == SVINFO ) out += "SVINFO" ;
        else if( msg_id == TIMEGPS ) out += "TIMEGPS" ;
        else if( msg_id == TIMEUTC ) out += "TIMEUTC" ;
        else if( msg_id == VELECEF ) out += "VELECEF" ;
        else if( msg_id == VELNED ) out += "VELNED" ;
    }
    else if( msg_cl == RXM )
    {
        out += "RXM-" ;
        using namespace rxm ;

        if( msg_id == ALM ) out += "ALM" ;
        else if( msg_id == EPH ) out += "EPH" ;
        else if( msg_id == PMREQ ) out += "PMREQ" ;
        else if( msg_id == RAW ) out += "RAW" ;
        else if( msg_id == SFRB ) out += "SFRB" ;
        else if( msg_id == SVSI ) out += "SVSI" ;
    }
    else if( msg_cl == AID )
    {
        out += "AID-" ;
        using namespace aid ;

        if( msg_id == ALM ) out += "ALM" ;
        else if( msg_id == EPH ) out += "EPH" ;
        else if( msg_id == ALPSRV ) out += "ALPSRV" ;
        else if( msg_id == ALP ) out += "ALP" ;
        else if( msg_id == AOP ) out += "AOP" ;
        else if( msg_id == DATA ) out += "DATA" ;
        else if( msg_id == HUI ) out += "HUI" ;
        else if( msg_id == INI ) out += "INI" ;
        else if( msg_id == REQ ) out += "REQ" ;
    }

    return out ;
}

buffer_t generatePollRequest( const U1 & m_cl , const U1 & m_id )
{
    buffer_t buf ;

    buf.assign( HEADER_SIZE+CRC_SIZE , char( 0 ) );
    buf[0] = SYNC1 ;
    buf[1] = SYNC2 ;
    buf[2] = m_cl ;
    buf[3] = m_id ;
    buf[4] = 0 ;
    buf[5] = 0 ;
    computeCRC( buf[6] , buf[7] , &buf );

    return buf ;
}


} // end namespace ublox
